from mlxtend.data import loadlocal_mnist
import matplotlib.pyplot as plt
X, y = loadlocal_mnist(
    images_path='train-images-idx3-ubyte',
    labels_path='train-labels-idx1-ubyte')
# print('Dimensions: %s x %s' % (X.shape[0], X.shape[1]))
# print('\n1st row', X[0])

import numpy as np
import function as fun
from scipy.optimize import minimize
input_size = 784
hidden_size = 25
num_labels = 10
learning_rate = 1
y = np.matrix(y)
y = y.T
params = (np.random.random(size=hidden_size * (input_size + 1) +
                           num_labels * (hidden_size + 1)) - 0.5) * 0.25


fmin = minimize(fun=fun.backprop, x0=params, args=(input_size, hidden_size, num_labels, X, y, learning_rate),
                method='TNC', jac=True, options={'maxiter': 250})


np.savetxt('param.txt', fmin.x, delimiter=',')

