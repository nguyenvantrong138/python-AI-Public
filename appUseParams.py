import numpy as np
import matplotlib.pyplot as plt
import function as fun
from mlxtend.data import loadlocal_mnist
X, y = loadlocal_mnist(
    images_path='t10k-images-idx3-ubyte',
    labels_path='t10k-labels-idx1-ubyte')
input_size = 784
hidden_size = 25
num_labels = 10
y = np.matrix(y)
y = y.T
fmin = np.loadtxt('param.txt')

X = np.matrix(X)
theta1 = np.matrix(np.reshape(
    fmin[:hidden_size * (input_size + 1)], (hidden_size, (input_size + 1))))
theta2 = np.matrix(np.reshape(
    fmin[hidden_size * (input_size + 1):], (num_labels, (hidden_size + 1))))

a1, z2, a2, z3, h = fun.forward_propagate(X, theta1, theta2)
y_pred = np.array(np.argmax(h, axis=1) + 1)
print(y_pred)
print(y)
correct = [1 if a == b else 0 for (a, b) in zip(y_pred, y)]
accuracy = (sum(map(int, correct)) / float(len(correct)))
print('accuracy = {0}%'.format(accuracy * 100))

while 1:
    trong = np.random.randint(0, X.shape[0] - 1)
    print("pred", y_pred[trong])
    img = np.reshape(X[trong], (28, 28), order='C')
    plt.figure(figsize=(3, 3))
    plt.imshow(img)
    plt.show()
    i = input("nhap q neu muon stop")
    if(i == "q"):
        break
    print(i)
